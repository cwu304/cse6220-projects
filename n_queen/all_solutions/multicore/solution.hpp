/*A headfile for solution
 *Declare variables and functions
 */
#ifndef SOLUTION_H
#define SOLUTION_H

#include <bitset>
#include <iostream>
#include <fstream>
#include <mpi.h>
#include <queue>
#include <stack>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <vector>
typedef unsigned int unsgn;
typedef unsigned long pos_t;
//typedef MPI_UNSIGNED_LONG MPI_pos_t;

using namespace std;

const unsgn MASTER=0;
const unsgn SEND_AT = 5;
//DFS, when reached this level, the master processor sends the idle slave 3 info:
//column, leftdiag, rightdiag
const unsgn BR_SIZE=128*2; //This is the buffer size, adjust me for increasing N

class Solution{

public:
  //Solution();
  void printResult();//print the result in good format to solutions.txt
  static unsgn N;//size of the board
  unsgn N_processors;//number of the processors, obtained from setRank
  void generateResult(queue<unsgn> &idle);//generate the result and store within the object
  static pos_t everywhere;//N 1's as binary to represent all positions are good and remove bits out of N
  void slaveProcess();//the slave process the task sent from the master and knows when to terminate
  unsgn rank;//store the rank
  void setRank(unsgn r);//set the rank
  void setNProcessors(unsgn np);//set the # of the processors

private:
  stack<vector<pos_t> > result;//store the results

};

#endif
