/**
this is the driver function containing the main function
 **/
#include "solution.hpp"

int main(int argc, char** argv){
  
  int rank=0;//rank of the processor
  int np=0;//# of the processors
  char hostname[MPI_MAX_PROCESSOR_NAME+1];
  int namelen=0;

  MPI_Init(&argc,&argv);
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);//getting the rank of the processor
  MPI_Comm_size(MPI_COMM_WORLD,&np);//getting # of the processors
  MPI_Get_processor_name(hostname,&namelen);

  if(rank==(int)MASTER){
    cout<<"this is the master " << hostname << "; I am the "<< rank << " out of " << np << endl;
    queue<unsgn> idle;//to keep the idle processors
    //push node 1,2,..., np to idle
    for(int i=1; i<np; i++){
      idle.push(i);
    }
    //creating an instance of Solution and set the rank for master
    Solution mySolver;  mySolver.setRank(rank); mySolver.setNProcessors(np);
    double starttime,endtime;//to mark the time
    starttime=MPI::Wtime();
    mySolver.generateResult(idle);//solutions are generated and stored within the class
    endtime=MPI::Wtime();
    ofstream ofs("time.txt",ofstream::app);//write to time.txt
    ofs<<"Running time "<<endtime-starttime<<endl;
    ofs.close();
    mySolver.printResult();

  }else{
    cout<<"from slave process " << rank << ":" << hostname << endl;
    Solution slaveSolver;
    slaveSolver.setRank(rank);  slaveSolver.setNProcessors(np);
    slaveSolver.slaveProcess();

  }
  
  MPI_Finalize();
  
}
