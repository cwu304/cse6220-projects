#ifndef SOLUTION_H
#define SOLUTION_H

#include <bitset>
#include <iostream>
#include <fstream>
#include <mpi.h>
#include <queue>
#include <stack>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <vector>
typedef unsigned int unsgn;
typedef unsigned long pos_t;
//typedef MPI_UNSIGNED_LONG MPI_pos_t;

using namespace std;

const unsgn MASTER=0;
const unsgn SEND_AT = 5;
//DFS, when reached this level, the master processor sends the idle slave 3 info:
//column, leftdiag, rightdiag
const unsgn BR_SIZE=32;

class Solution{

public:
  //Solution();
  void printResult();
  static unsgn N;
  unsgn N_processors;
  void generateResult(queue<unsgn> &idle);
  static pos_t everywhere;
  void slaveProcess();
  unsgn rank;
  void setRank(unsgn r);
  void setNProcessors(unsgn np);

private:
  stack<vector<pos_t> > result;

};

#endif
