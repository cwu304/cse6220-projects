#include "solution.hpp"

void Solution::printResult(){
  char filename[32];
  strcpy(filename,"solutions.txt"); 
  ofstream ofs(filename,ofstream::out);
  ofs<<"Settings:" << endl;
  ofs<<"# of processors="<<N_processors<<"\tN="<<N<<"\tsend at " <<SEND_AT<<endl;
  ofs<<"there are " << result.size() << " solutions" << endl;
  while(!result.empty()){
    vector<pos_t> ans(result.top());
    for(unsgn i=0; i<ans.size(); i++){
      bitset<8> bitdisp(ans[i]);
      ofs<<bitdisp<<endl;
    }
    ofs << endl;
    result.pop();
  }
  ofs.close();
}

unsgn Solution::N=8;
pos_t Solution::everywhere=(1<<N)-1;

void Solution::setNProcessors(unsgn np){
  N_processors=np;
}

void Solution::generateResult(queue<unsgn>& idle){
  stack<vector<pos_t> > board;
  stack<pos_t> column;
  stack<pos_t> leftdiag;
  stack<pos_t> rightdiag;
  
  vector<pos_t> nothing;
  column.push(everywhere);
  board.push(nothing); leftdiag.push(everywhere);  rightdiag.push(everywhere);
  
  unsgn solutionCount=0;
  
  while(!board.empty()){
    vector<pos_t> done(board.top());  board.pop();
    pos_t occupiedColumn=column.top();  column.pop();
    pos_t occupiedLD=leftdiag.top();  leftdiag.pop();
    pos_t occupiedRD=rightdiag.top(); rightdiag.pop();
    pos_t available=(occupiedColumn & (occupiedLD & occupiedRD)) & everywhere;

    if(done.size()==SEND_AT){
      //result.push(done);
      //send occupiedLD, occupedRD and occupiedColumn to the next idle node;
      if(idle.empty()){
	//have to wait until there is at least one slave free to use
	pos_t branchAns[1+N]; MPI_Status status;
	MPI_Recv(branchAns,1+N,MPI_UNSIGNED_LONG,MPI_ANY_SOURCE,0xEEEE, MPI_COMM_WORLD,&status);
	unsgn p=1;
	for(unsgn i=0; i<(unsgn)*branchAns; i++){
	  vector<pos_t> tmp;
	  for(unsgn j=0; j<N; j++){
	    tmp.push_back(*(branchAns+p));  p++;
	  }				       
	  result.push(tmp);
	  //	  break;
	}
	//	cout<<"receiving response from rank." << status.MPI_SOURCE << " with " << initInfo << " solutions" << endl;
	idle.push(status.MPI_SOURCE);  solutionCount+=*(branchAns);
	if(*branchAns>0){
	  unsgn sendsize=SEND_AT+3;//end the signal to tell the slaves to rest;
	  pos_t endbuf[sendsize];
	  endbuf[0]=everywhere; endbuf[1]=everywhere; endbuf[2]=everywhere;
	  MPI_Send(&endbuf,sendsize, MPI_UNSIGNED_LONG,status.MPI_SOURCE,0xBEEF,MPI_COMM_WORLD);     
	  cout << "got at least one solution" << endl;
	  break;
	}
	
      }
      
      pos_t nextIdle=idle.front();  idle.pop();
      unsgn sendsize=SEND_AT+3;
      pos_t buf[sendsize];
      buf[0]=occupiedColumn; buf[1]=occupiedLD;  buf[2]=occupiedRD;
      for(unsgn i=0; i<SEND_AT; i++){
	buf[i+3]=done[i];
      }
      MPI_Send(&buf,sendsize, MPI_UNSIGNED_LONG,nextIdle,0xBEEF,MPI_COMM_WORLD);
      
    }else{
      for(unsgn i=0; i<N; i++){
	pos_t mask=1<<i;
	pos_t empty=(available & mask);
	if(empty){
	  vector<pos_t> updated(done);  updated.push_back(mask);
	  pos_t updatedColumn=occupiedColumn&(~mask);
	  pos_t updatedLD=occupiedLD&(~mask);  updatedLD=((updatedLD<<1)+1)&everywhere;
	  pos_t updatedRD=occupiedRD&(~mask);  updatedRD=((updatedRD>>1)+(1<<(N-1)))&everywhere;
	  board.push(updated);  column.push(updatedColumn);  leftdiag.push(updatedLD);  rightdiag.push(updatedRD);
	}

      }
    }

  }

  while(idle.size()<N_processors-1){
    pos_t branchAns[1+N]; MPI_Status status;
    MPI_Recv(branchAns,1+N,MPI_UNSIGNED_LONG,MPI_ANY_SOURCE,0xEEEE, MPI_COMM_WORLD,&status);
    
    idle.push(status.MPI_SOURCE);  solutionCount+=*(branchAns);
    cout << "ending " << status.MPI_SOURCE<<" though he has got " << *branchAns << " solutions" << endl;
    cout<<"now idle has " << idle.size() << " elements" << endl;
    unsgn sendsize=SEND_AT+3;//end the signal to tell the slaves to rest;
    pos_t endbuf[sendsize];
    endbuf[0]=everywhere; endbuf[1]=everywhere; endbuf[2]=everywhere;
    MPI_Send(&endbuf,sendsize, MPI_UNSIGNED_LONG,status.MPI_SOURCE,0xBEEF,MPI_COMM_WORLD);     
    
  }


  cout<<idle.size() << " processors are idle" << endl;
  cout<<solutionCount << " solutions generated" << endl;
}

void Solution::setRank(unsgn r){
  rank=r;
}

void Solution::slaveProcess(){
  MPI_Status status;
  while(true){
    pos_t columnInfo=0, LDinfo=0, RDinfo=0;
    unsgn sendsize=SEND_AT+3;
    pos_t buf[sendsize];
    MPI_Recv(&buf,sendsize,MPI_UNSIGNED_LONG,MASTER,0xBEEF,MPI_COMM_WORLD,&status);
    columnInfo=buf[0];  LDinfo=buf[1];  RDinfo=buf[2];
    if(columnInfo==everywhere && LDinfo==everywhere && RDinfo==everywhere){
      break;
    }
    
    vector<pos_t> init;
    for(unsgn i=3; i<sendsize;i++){
      init.push_back(buf[i]);
    }

    stack<vector<pos_t> > board;  board.push(init);
    stack<pos_t> leftdiag;  leftdiag.push(LDinfo);
    stack<pos_t> rightdiag;  rightdiag.push(RDinfo);
    stack<pos_t> column;  column.push(columnInfo);
    
    vector<vector<pos_t> > tmp_reserve;
    for(unsgn cnt=0; cnt<100000 && !board.empty(); cnt++){//this is a problem!
      //here the possible paths are too many! as a result, it cannot end normally,
      //so I add this special bad criterion on it!
      vector<pos_t> done(board.top());  board.pop();
      pos_t occupiedColumn=column.top();  column.pop();
      pos_t occupiedLD=leftdiag.top();  leftdiag.pop();
      pos_t occupiedRD=rightdiag.top(); rightdiag.pop();
      pos_t available=(occupiedColumn & (occupiedLD & occupiedRD)) & everywhere;

      if(done.size()==N){
	tmp_reserve.push_back(done);
	//	cout << "node." << rank << " found a solution\n"<<endl;
	break;
      }else{
	//	cout << "size of the stack = " << board.size() << endl;
	for(unsgn i=0; i<N; i++){
	  pos_t mask=1<<i;
	  pos_t empty=(available & mask);
	  if(empty){
	    vector<pos_t> updated(done);  updated.push_back(mask);
	    pos_t updatedColumn=occupiedColumn&(~mask);
	    pos_t updatedLD=occupiedLD&(~mask);  updatedLD=((updatedLD<<1)+1)&everywhere;
	    pos_t updatedRD=occupiedRD&(~mask);  updatedRD=((updatedRD>>1)+(1<<(N-1)))&everywhere;
	    board.push(updated);  column.push(updatedColumn);  leftdiag.push(updatedLD);  rightdiag.push(updatedRD);
	  }

	}
      }
    }
    
    cout<< "node." << rank << " discovered " << tmp_reserve.size() << " solutions" <<endl;
    pos_t branchAns[1+N];
    *(branchAns)=(pos_t)tmp_reserve.size();
    unsgn p=1;
    for(unsgn i=0; i<tmp_reserve.size(); i++){
      for(unsgn j=0; j<N;j++){
	*(branchAns+p)=tmp_reserve[i][j]; p++;
      }
    }
    MPI_Send(branchAns, 1+N, MPI_UNSIGNED_LONG, MASTER, 0xEEEE, MPI_COMM_WORLD);

  }

  cout<<"node." << rank << " finished all my job" << endl;
  //  printResult();
}
