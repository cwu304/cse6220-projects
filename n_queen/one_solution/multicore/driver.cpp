#include "solution.hpp"

int main(int argc, char** argv){
  
  int rank=0;
  int np=0;
  char hostname[MPI_MAX_PROCESSOR_NAME+1];
  int namelen=0;

  MPI_Init(&argc,&argv);
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  MPI_Comm_size(MPI_COMM_WORLD,&np);
  MPI_Get_processor_name(hostname,&namelen);

  if(rank==(int)MASTER){
    cout<<"this is the master " << hostname << "; I am the "<< rank << " out of " << np << endl;
    queue<unsgn> idle;//to keep the idle processors
    for(int i=1; i<np; i++){
      idle.push(i);
    }
    Solution mySolver;  mySolver.setRank(rank); mySolver.setNProcessors(np);
    double starttime,endtime;
    starttime=MPI::Wtime();
    mySolver.generateResult(idle);
    endtime=MPI::Wtime();
    ofstream ofs("time.txt",ofstream::app);
    ofs<<"Running time "<<endtime-starttime<<endl;
    ofs.close();
    mySolver.printResult();

  }else{
    cout<<"from slave process " << rank << ":" << hostname << endl;
    Solution slaveSolver;
    slaveSolver.setRank(rank);  slaveSolver.setNProcessors(np);
    slaveSolver.slaveProcess();

  }
  
  MPI_Finalize();
  
}
