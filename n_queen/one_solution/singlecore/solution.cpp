#include "solution.hpp"

void Solution::printResult(){
  ofstream ofs("solutions.txt", ofstream::out);
  ofs<<"settings:" << endl;
  ofs<<"sequential algorithm\tN="<<N<<endl;
  ofs<<"there are " << result.size() << " solutions" << endl;
  while(!result.empty()){
    vector<pos_t> ans(result.top());
    for(unsgn i=0; i<ans.size(); i++){
      bitset<8> bitdisp(ans[i]);
      ofs<<bitdisp<<endl;
    }
    ofs << endl;
    result.pop();
  }
  ofs.close();
}

unsgn Solution::N=8;//default size;
pos_t Solution::everywhere=(1<<N)-1;

void Solution::generateResult(){
  stack<vector<pos_t> > board;
  stack<pos_t> column;
  stack<pos_t> leftdiag;
  stack<pos_t> rightdiag;
  
  vector<pos_t> nothing;
  column.push(everywhere);
  board.push(nothing); leftdiag.push(everywhere);  rightdiag.push(everywhere);

  while(!board.empty()){
    vector<pos_t> done(board.top());  board.pop();
    pos_t occupiedColumn=column.top();  column.pop();
    pos_t occupiedLD=leftdiag.top();  leftdiag.pop();
    pos_t occupiedRD=rightdiag.top(); rightdiag.pop();
    pos_t available=(occupiedColumn & (occupiedLD & occupiedRD)) & everywhere;

    if(done.size()==N){
      result.push(done);
      cout<<"one solution found"<<endl;
      break;
    }else{
      for(unsgn i=0; i<N; i++){
	pos_t mask=1<<i;
	pos_t empty=(available & mask);
	if(empty){
	  vector<pos_t> updated(done);  updated.push_back(mask);
	  pos_t updatedColumn=occupiedColumn&(~mask);
	  pos_t updatedLD=occupiedLD&(~mask);  updatedLD=((updatedLD<<1)+1)&everywhere;
	  pos_t updatedRD=occupiedRD&(~mask);  updatedRD=((updatedRD>>1)+(1<<(N-1)))&everywhere;
	  board.push(updated);  column.push(updatedColumn);  leftdiag.push(updatedLD);  rightdiag.push(updatedRD);
	}

      }
    }

  }
  
}
