#ifndef SOLUTION_H
#define SOLUTION_H

#include <bitset>
#include <fstream>
#include <iostream>
#include <mpi.h>
#include <stack>
#include <vector>
typedef unsigned int unsgn;
typedef unsigned long pos_t;

using namespace std;
class Solution{

public:
  //Solution();
  void printResult();
  static unsgn N;
  void generateResult();
  void generateResult(pos_t row,pos_t leftdiag, pos_t rightdiag);
  static pos_t everywhere;

private:
  stack<vector<pos_t> > result;

};

#endif
