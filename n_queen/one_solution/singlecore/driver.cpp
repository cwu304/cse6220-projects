
#include "solution.hpp"

int main(int argc, char** argv){
  double starttime,endtime;
  Solution mySolver;
  starttime=MPI::Wtime();
  mySolver.generateResult();
  endtime=MPI::Wtime();
  ofstream ofs("time.txt",ofstream::out);
  ofs<<"runtime is " << endtime-starttime<<endl;
  ofs.close();
  mySolver.printResult();
}
